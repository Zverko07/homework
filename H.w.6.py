# Напишите функцию которая принемает *args
masiv = [
    {
        'name': 'Adil',
        'age': 16
    },
    {
        'name': 'Malik',
        'age': 25
    }
]

def arr(*args):
    print(args)

arr(masiv)

# Напишите функцию которая принемает *kwargs
def arr(**kwargs):
    print(kwargs)

arr(stoit=15)

# Напишите функцию которая принемает *args и проходит циклом по пришедшмим данным
def arr(*args):
    for i in args:
        for god in i:
            import time
            print(god)
            time.sleep(2)

arr(masiv)

# Напишите имитацию работу клуба (массив девушек, массив парней, охраник, администратор), но с использованием
# параметров args, kwargs
boys = [
    {
        'name': 'Kamila',
        'age': 16
    },
    {
        'name': 'Alisa',
        'age': 25
    },
]

girls = [
    {
        'name': 'Islam',
        'age': 16
    },
    {
        'name': 'Malik',
        'age': 25
    }
]


def arr(*args, **kwargs):
    for i in args:
        for god in i:
            if god["age"] >= 18:
                import time
                print(kwargs["secure"] + ':' + god["name"] + " проходишь ")
                print(kwargs['admin'] + ": поставил печать " + god['name'], '\n')
                time.sleep(2)
            else:
                print(kwargs['secure'] + ":" + god['name'] + " э иди спать ой бой!" + "\n")


arr(boys,girls, secure="Jack", admin="Adil")
